package com.example.blackbriar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyGroupFragment extends Fragment {

    View view;
    TextView txtTitleForum;
    String title;

    public MyGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_group, container, false);{
            Variables titleForum =(Variables) getApplicationContext();
            txtTitleForum = view.findViewById(R.id.txtTitleForum);
            title = titleForum.getTitleGroup();
            txtTitleForum.setText(title);
        }
        // Inflate the layout for this fragment
        return view;
    }

}
