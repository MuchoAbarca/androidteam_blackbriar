package com.example.blackbriar;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.blackbriar.model.Usuario;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;


import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    View view;
    Button btnSigup, btnLogin;
    SignInButton btnLoginGoogle;
    EditText edTUser,edPassword;

    private CallbackManager mCallbackManager;
    private static final String TAG = "FacebookLogin";


    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;

    private FirebaseAuth mAuth;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {


        //Infla fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        {
            mAuth = FirebaseAuth.getInstance();
            //Google
            // Configure Google Sign In
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestProfile()
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
            btnLoginGoogle =view.findViewById(R.id.btnLoginGoogle);
            btnLoginGoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signIn();
                }
            });
            //Button SignUp
            btnSigup = view.findViewById(R.id.btnSignup);
            btnSigup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,
                            new SignUpFragment(), null).addToBackStack(null).commit();
                }
            });//Button Login

            edTUser = view.findViewById(R.id.eTxtUsername);
            edPassword = view.findViewById(R.id.eTxtPassword);
            btnLogin = view.findViewById(R.id.btnLogin);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getId()==R.id.btnLogin) {
                        LoginUser(edTUser.getText().toString(),edPassword.getText().toString());
                    }

              }
                private void LoginUser(final String email, String password){
                    if(TextUtils.isEmpty(email)){
                        Toast.makeText(getActivity(),"Debe de ingresar un email ",Toast.LENGTH_LONG).show();
                        return;
                    }
                    if(TextUtils.isEmpty(password)){
                        Toast.makeText(getActivity(),"Falta ingresar la contraseña",Toast.LENGTH_LONG).show();
                        return;
                    }
                    mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                                FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                                Intent intent = new Intent(getContext(),Home.class);
                                startActivity(intent);

                            }
                            else {
                                Toast.makeText(getActivity(),"El usuario no existe o la contraseña es erronea", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });
                }
            //Button Login
         });

            //Facebook login
            mCallbackManager = CallbackManager.Factory.create();
            LoginButton loginButton = view.findViewById(R.id.login_button);
            loginButton.setReadPermissions("email", "public_profile");
            loginButton.setFragment(this);
            loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d(TAG, "facebook:onSuccess:" + loginResult);
                    handleFacebookAccessToken(loginResult.getAccessToken());

                }
                @Override
                public void onCancel() {
                    Log.d(TAG, "facebook:onCancel");
                    updateUI(null);
                }
                @Override
                public void onError(FacebookException error) {
                    Log.d(TAG, "facebook:onError", error);
                    updateUI(null);

                }
            });

        }
        // Inflate the layout for this fragment
        return view;

    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplicationContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("", "Google sign in failed", e);
                // ...
            }
        }
    }
    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener( new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                    Intent intent = new Intent(getContext(),Home.class);
                    startActivity(intent);

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("", "signInWithCredential:failure", task.getException());

                    updateUI(null);
                }
            }
        });

    }
    //google
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct){
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(),null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("", "signInWithCredential:success");
                            Intent intent = new Intent(getContext(),Home.class);
                            startActivity(intent);
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("", "signInWithCredential:failure", task.getException());
                            updateUI(null);
                        }

                    }
                });
    }
    //login google
    //Actualiza el usuario que esta en login
    private void updateUI(final FirebaseUser user) {
        if (user != null) {

            final String User = user.getUid();
            final String Email = user.getEmail();
            final String Name = user.getDisplayName();
            Usuario usuario = new Usuario();
            usuario.setUid(User);
            usuario.setName(Name);
            usuario.setEmail(Email);
            db.collection("User").document(User).set(usuario);
            Intent intent = new Intent(getContext(), Home.class);
            startActivity(intent);
        }

    }

}
