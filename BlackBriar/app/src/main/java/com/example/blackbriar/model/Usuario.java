package com.example.blackbriar.model;

import android.net.Uri;

public class Usuario {
    private  String uid;
    private Uri photo;
    private String name;
    private String lastName;
    private String email;
    private String password;

    public Usuario(String uid, Uri photo, String name, String lastName, String email, String password) {
        this.uid = uid;
        this.photo = photo;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public Usuario(){

    }
    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
