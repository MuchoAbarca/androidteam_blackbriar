package com.example.blackbriar;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.blackbriar.model.Group;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends Fragment {

    View view;
    RecyclerView rv;


    List<Group> groups;
    Adapter adapter;
    FirebaseFirestore db;

    public GroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_group, container, false);{
            db = FirebaseFirestore.getInstance();
            rv = view.findViewById(R.id.recycle);
            rv.setHasFixedSize(true);
            rv.setLayoutManager(new LinearLayoutManager(getContext()));
            groups = new ArrayList<>();
            adapter = new Adapter(groups);
            rv.setAdapter(adapter);

            db.collection("Group").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            Group group = document.toObject(Group.class);
                            groups.add(group);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            });

        }
        // Inflate the layout for this fragment
        return view;
    }


}
