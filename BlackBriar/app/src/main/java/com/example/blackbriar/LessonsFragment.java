package com.example.blackbriar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.blackbriar.model.Group;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class LessonsFragment extends Fragment {

    View view;
    RecyclerView rv;


    List<Group> groups;
    Adapter adapter;
    FirebaseFirestore db;



    public LessonsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lessons, container, false);{

        }
        // Inflate the layout for this fragment
        return view;
    }

}
