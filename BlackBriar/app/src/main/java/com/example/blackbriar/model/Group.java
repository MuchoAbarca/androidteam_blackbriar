package com.example.blackbriar.model;

public class Group {
    private String uid;
    private String name;
    private int cantidadAlumnos;
    private int registrados;
    private String propietario;
    private Boolean privado;
    private int pin;


    public Group(String uid, String name, int cantidadAlumnos, int registrados, String propietario, Boolean privado, int pin) {
        this.uid = uid;
        this.name = name;
        this.cantidadAlumnos = cantidadAlumnos;
        this.registrados = registrados;
        this.propietario = propietario;
        this.privado = privado;
        this.pin = pin;
    }

    public Group() {

    }

    public Boolean getPrivado() { return privado; }

    public void setPrivado(Boolean privado) {
        this.privado = privado;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCantidadAlumnos() {
        return cantidadAlumnos;
    }

    public void setCantidadAlumnos(int cantidadAlumnos) {
        this.cantidadAlumnos = cantidadAlumnos;
    }

    public int getRegistrados() {
        return registrados;
    }

    public void setRegistrados(int registrados) {
        this.registrados = registrados;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public int getPin() {
        return pin; }

    public void setPin(int pin) { this.pin = pin; }
}
