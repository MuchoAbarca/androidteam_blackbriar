package com.example.blackbriar;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment {

    View view;
    FloatingActionButton fab, fabForum,fabclose, fabClassroom, fabGroup, fabScore;


    public ForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forum, container, false);
        {

            fabClassroom = view.findViewById(R.id.btnClassroom);
            fabScore = view.findViewById(R.id.btnScore);
            fabGroup = view.findViewById(R.id.btnGroup);
            fab = view.findViewById(R.id.btnAdd);
            fabclose = view.findViewById(R.id.btnClose);
            fabForum = view.findViewById(R.id.btnAddForum);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fab.hide();
                    fabClassroom.show();
                    fabScore.show();
                    fabGroup.show();
                    fabForum.show();
                    fabclose.show();
                    fabClassroom.show();
                }
            });
            fabclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeFabMenu();
                }
            });
            fabForum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_forum,
                            new AddForumFragment(), null).addToBackStack(null).commit();
                }
            });
            fabClassroom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_forum,
                            new HomeForumFragment(), null).addToBackStack(null).commit();
                }
            });
            fabScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_forum,
                            new ScoreboardFragment(), null).addToBackStack(null).commit();
                }
            });
            fabGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_forum,
                            new MyGroupFragment(), null).addToBackStack(null).commit();
                }
            });
        }
        return view;
    }

    private void closeFabMenu(){
        fab.show();
        fabclose.hide();
        fabForum.hide();
        fabClassroom.hide();
        fabScore.hide();
        fabGroup.hide();
        fabClassroom.hide();
    }
}
