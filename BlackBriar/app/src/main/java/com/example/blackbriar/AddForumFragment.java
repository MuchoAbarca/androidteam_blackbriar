package com.example.blackbriar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.blackbriar.model.Forum;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddForumFragment extends Fragment {
    View view;
    EditText dateStart, dateEnd, txtName, txtDesc, txtHea, txtWar ;
    Button btnCreateForum;
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    FirebaseFirestore db;

    public AddForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_forum, container, false);{
            mAuth = FirebaseAuth.getInstance();
            currentUser = mAuth.getCurrentUser();
            db = FirebaseFirestore.getInstance();
            dateStart = view.findViewById(R.id.eTxtDateStart);
            dateEnd = view.findViewById(R.id.eTxtDateEnd);
            btnCreateForum = view.findViewById(R.id.btnCreate);
            txtName = view.findViewById(R.id.eTxtNameForum);
            txtDesc = view.findViewById(R.id.eTxtDescription);
            txtHea = view.findViewById(R.id.eTxtHealer);
            txtWar = view.findViewById(R.id.eTxtWarlok);
            btnCreateForum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                addForum();
                }
            });
            Date date = new Date();
            dateStart.setText(DateFormat.getDateInstance(DateFormat.SHORT).format(date));
            new DateEdittext(dateEnd);
        }


        // Inflate the layout for this fragment
        return view;
    }

 private void addForum(){
     Variables uidForum =(Variables) getApplicationContext();
     String group = uidForum.getUidForum();
     String uid =(UUID.randomUUID().toString());
     String Name = txtName.getText().toString();
     String Description = txtDesc.getText().toString();
     String Inicio = dateStart.getText().toString();
     String Final = dateEnd.getText().toString();
     int Healer = Integer.parseInt(txtHea.getText().toString());
     int Warlock = Integer.parseInt(txtWar.getText().toString());
     Forum forum  = new Forum();
     forum.setNameForum(Name);
     forum.setDescripción(Description);
     forum.setDiaInicio(Inicio);
     forum.setDiaFinal(Final);
     forum.setPointsHealer(Healer);
     forum.setPointsWarlock(Warlock);
     forum.setUidForum(uid);
     db.collection("Group").document(group).collection("Forum").document(uid).set(forum);
     Toast.makeText(getContext(),"Forum created correctly",Toast.LENGTH_LONG).show();
 }
}
