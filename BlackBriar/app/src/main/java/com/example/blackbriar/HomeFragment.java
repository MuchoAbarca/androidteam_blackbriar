package com.example.blackbriar;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    FloatingActionButton fab,fabgroup,fabclose,fabAddgroup;
    TextView txtJoin,txtAddGroup;

    View view;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);{

            txtJoin = view.findViewById(R.id.txtJoin);
            txtAddGroup = view.findViewById(R.id.txtAddGroup);
            fabAddgroup = view.findViewById(R.id.btnClassroom);
            fab = view.findViewById(R.id.btnAdd);
            fabclose = view.findViewById(R.id.btnClose);
            fabgroup = view.findViewById(R.id.btnAddForum);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtJoin.setVisibility(View.VISIBLE);
                    txtAddGroup.setVisibility(View.VISIBLE);
                    fab.hide();
                    fabgroup.show();
                    fabclose.show();
                    fabAddgroup.show();
                }
            });
            fabclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtJoin.setVisibility(View.INVISIBLE);
                    txtAddGroup.setVisibility(View.INVISIBLE);
                    fab.show();
                    fabclose.hide();
                    fabgroup.hide();
                    fabAddgroup.hide();
                }
            });
            fabgroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                            new GroupFragment(), null).addToBackStack(null).commit();
                }
            });
            fabAddgroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                            new AddGroupFragment(), null).addToBackStack(null).commit();
                }
            });
        }
        return view;
    }

}
