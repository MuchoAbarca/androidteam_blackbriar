package com.example.blackbriar;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



public class MainActivity extends AppCompatActivity {

    public static FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        fragmentManager=getSupportFragmentManager();
        //Overalping fragment if not
        if(savedInstanceState!=null){
            return;
        }
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        // se crea objeto homeFragment que se agrega al fragment_conteiner
        LoginFragment loginFragment= new LoginFragment();
        fragmentTransaction.add(R.id.fragment_container,loginFragment,null);
        fragmentTransaction.commit();

    }

}


