package com.example.blackbriar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blackbriar.model.Group;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddGroupFragment extends Fragment {

View view;
Button btnBack,btnMake;
TextView txtPin;
EditText eTxtNameGroup, eTxtN, eTxtPin;
Switch privateGroup;
private FirebaseAuth mAuth;
FirebaseUser currentUser;
FirebaseFirestore db;

    public AddGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_group, container, false);{
            mAuth = FirebaseAuth.getInstance();
            currentUser = mAuth.getCurrentUser();
            db = FirebaseFirestore.getInstance();
            eTxtNameGroup = view.findViewById(R.id.eTxtGroupName);
            eTxtPin = view.findViewById(R.id.eTxtPin);
            eTxtN = view.findViewById(R.id.eTxtCantidad);
            txtPin = view.findViewById(R.id.txtPin);
            privateGroup = view.findViewById(R.id.switchGroup);
            privateGroup.setChecked(false);
            btnBack = view.findViewById(R.id.btnBack2);
            btnMake = view.findViewById(R.id.btnMakeGroup);
            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                            new HomeFragment(), null).addToBackStack(null).commit();
                }
            });
            eTxtNameGroup = view.findViewById(R.id.eTxtGroupName);
            eTxtN = view.findViewById(R.id.eTxtCantidad);
            btnMake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addGroup();
                    Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                            new ClassroomFragment(), null).addToBackStack(null).commit();
                }
            });
            privateGroup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        eTxtPin.setVisibility(View.VISIBLE);
                        txtPin.setVisibility(View.VISIBLE);
                    }else{
                        eTxtPin.setVisibility(View.INVISIBLE);
                        txtPin.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
        // Inflate the layout for this fragment
        return view;

    }

    private void addGroup(){
        String name = eTxtNameGroup.getText().toString();
        String propietario = currentUser.getUid();
        int cantidadAlumnos = Integer.parseInt(eTxtN.getText().toString());
        int  PIN = Integer.parseInt(eTxtPin.getText().toString());
        Group group = new Group();
        group.setUid(UUID.randomUUID().toString());
        group.setName(name);
        group.setCantidadAlumnos(cantidadAlumnos);
        group.setPropietario(propietario);
        group.setPrivado(privateGroup.isChecked());
        group.setPin(PIN);
        db.collection("Group").document(group.getUid()).set(group);
        Toast.makeText(getContext(),"Group created correctly",Toast.LENGTH_LONG).show();
        eTxtNameGroup.setText("");
        eTxtN.setText("");
        eTxtPin.setText("");

    }

}
