package com.example.blackbriar;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    private EditText edCorreo,edPasssword, edConfirmPassword;
    Button BtnBack;
    Button BtnNext;
    View view;
    private FirebaseAuth mAuth;

    public SignUpFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        {
            //Firebase
            mAuth = FirebaseAuth.getInstance();
            edCorreo = view.findViewById(R.id.eTxtUsername);
            edPasssword = view.findViewById(R.id.eTxtPassword);
            edConfirmPassword = view.findViewById(R.id.eTxtConfirmPassword);
            BtnNext = view.findViewById(R.id.btnNext);
            BtnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registrarUsuario();

                }
            });

            BtnBack = view.findViewById(R.id.btnBack);
            BtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,
                            new LoginFragment(),null).addToBackStack(null).commit();
                }
            });
        }

        // Inflate the layout for this fragment
        return view;
    }
    private void registrarUsuario(){
        String Email = edCorreo.getText().toString().trim();
        String Password = edPasssword.getText().toString().trim();
        String ConfirmPassword = edConfirmPassword.getText().toString().trim();
        if(!Password.equals(ConfirmPassword)){
            edPasssword.getText().clear();
            edConfirmPassword.getText().clear();
            Toast.makeText(getContext(),"Passwords don't match",Toast.LENGTH_LONG).show();
        }
        else if(TextUtils.isEmpty(Email)){
            edCorreo.setError("Debe de ingresar un email");


        }
        else if(TextUtils.isEmpty(Password)){
            edPasssword.setError("Falta ingresar la contraseña");

        }else {
            mAuth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getActivity(), "Se ha registrado el usuario", Toast.LENGTH_LONG).show();
                        edCorreo.getText().clear();
                        edPasssword.getText().clear();
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);

                        MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,
                                new LoginFragment(),null).addToBackStack(null).commit();

                    } else {
                        if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                            Toast.makeText(getActivity(), "Este usuario ya existe", Toast.LENGTH_LONG).show();
                            edCorreo.getText().clear();
                            edPasssword.getText().clear();
                            updateUI(null);
                        }
                    }
                }

                private void updateUI(FirebaseUser user) {
                }
            });
        }
    }

}
