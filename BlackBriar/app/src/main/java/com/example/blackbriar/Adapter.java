package com.example.blackbriar;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blackbriar.model.Group;
import com.example.blackbriar.model.Lesson;
import com.example.blackbriar.model.Usuario;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import static java.lang.String.valueOf;


public class Adapter extends RecyclerView.Adapter<Adapter.GroupViewholder>{



    public Adapter(List<Group> groups) {
        this.groups = groups;
    }

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    FirebaseFirestore db;

    List<Group> groups;


    @NonNull
    @Override
    public GroupViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.groupcard,viewGroup,false);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        currentUser = mAuth.getCurrentUser();
        GroupViewholder holder = new GroupViewholder(v);
        return holder;

    }


    @Override
    public void onBindViewHolder(@NonNull final GroupViewholder groupViewholder, int i) {


        Group  group = groups.get(i);
        groupViewholder.txtName.setText(group.getName());
        groupViewholder.txtPropietario.setText(group.getPropietario());
        final String userNumber = valueOf(group.getCantidadAlumnos());
        final  String NameGroup = valueOf(group.getName());
        final String propietario = valueOf(group.getPropietario());
        final String uidLesson = valueOf(group.getUid());
        final String suscriptor  = currentUser.getUid();
        final String email = currentUser.getEmail();
        final String pinNumber = valueOf(group.getPin());
        String  userRegist = valueOf( group.getRegistrados());
        String total = userRegist  + "/" + userNumber ;
        final String typeOfGroup = valueOf(group.getPrivado());
        groupViewholder.txtNUsers.setText(total);
        if(typeOfGroup.equals("true") ){
            groupViewholder.privateView.setVisibility(View.VISIBLE);
        }
        groupViewholder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typeOfGroup.equals("false")) {
                    AlertDialogPublic(groupViewholder, NameGroup, propietario, uidLesson, suscriptor,email);
                }else{
                    AlertDialogPrivate(groupViewholder, pinNumber, NameGroup, propietario, uidLesson, suscriptor,email);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    public static class GroupViewholder extends RecyclerView.ViewHolder{

        TextView txtName, txtPropietario, txtNUsers;
        public CardView cardView;
        ImageView privateView;

        public GroupViewholder(@NonNull final View itemView) {
            super(itemView);

            privateView = itemView.findViewById(R.id.img_private);
            txtName = itemView.findViewById(R.id.eTxtDescription);
            txtPropietario = itemView.findViewById(R.id.txtusers);
            txtNUsers = itemView.findViewById(R.id.txtNUsers);
            cardView = itemView.findViewById(R.id.card_view);


        }

    }
    private void AlertDialogPublic
            (final GroupViewholder groupViewholder,
             final String NameGroup, final String propietario,
             final String uidLesson, final String  suscriptor,
            final String email){
        final AlertDialog.Builder subscribe = new AlertDialog.Builder(groupViewholder.cardView.getContext());
        subscribe.setMessage("Do you want to subscribe to " + NameGroup +" group?")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addLessonAndInscribed(NameGroup,propietario,uidLesson, suscriptor, email);
                        Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                new HomeFragment(), null).addToBackStack(null).commit();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog titulo = subscribe.create();
        titulo.setTitle("To subscribe");
        titulo.show();
    }
    private void AlertDialogPrivate
            (final GroupViewholder groupViewholder,
             final String pinNumber,
             final String NameGroup, final String propietario,
             final String uidLesson, final String  suscriptor,
             final String email){
        final AlertDialog.Builder subscribe = new AlertDialog.Builder(groupViewholder.cardView.getContext());
        Context context = subscribe.getContext();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        final View v = inflater.inflate(R.layout.pin, null);
        subscribe.setView(v)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText eTxtPin = v.findViewById(R.id.eTxtPin);
                        String pin = eTxtPin.getText().toString();
                        if (pin.equals(pinNumber)) {
                            addLessonAndInscribed(NameGroup,propietario,uidLesson, suscriptor, email);
                            Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                    new HomeFragment(), null).addToBackStack(null).commit();
                        } else {

                            Toast.makeText(groupViewholder.cardView.getContext(), "The pin is incorrect try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog pin = subscribe.create();
        pin.show();
    }
 private void addLessonAndInscribed(
         String NameGroup,
         String propietario,
         String uidLesson,
         String  suscriptor,
         String email){
     Usuario usuario = new Usuario();
     usuario.setUid(suscriptor);
     usuario.setEmail(email);
     Lesson lesson = new Lesson();
     lesson.setIdLesson(uidLesson);
     lesson.setName(NameGroup);
     lesson.setIdProfesor(propietario);
     db.collection("users").document(suscriptor).collection("Lessons").document(uidLesson).set(lesson);
     db.collection("Group").document(uidLesson).collection("Inscribed").document(suscriptor).set(usuario);
 }
}
