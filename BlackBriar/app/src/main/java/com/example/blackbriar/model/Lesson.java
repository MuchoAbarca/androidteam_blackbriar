package com.example.blackbriar.model;

public class Lesson {
    private String IdLesson;
    private String Name;
    private String IdProfesor;

    public Lesson(){

    }

    public Lesson(String idLesson, String name, String idProfesor) {
        IdLesson = idLesson;
        Name = name;
        IdProfesor = idProfesor;
    }

    public String getIdLesson() {
        return IdLesson;
    }

    public void setIdLesson(String idLesson) {
        IdLesson = idLesson;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIdProfesor() {
        return IdProfesor;
    }

    public void setIdProfesor(String idProfesor) {
        IdProfesor = idProfesor;
    }

}
