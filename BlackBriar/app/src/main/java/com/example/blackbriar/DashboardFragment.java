package com.example.blackbriar;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;



/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment  {

    View view;
    ImageView btnToolbar, userPhoto;
    DrawerLayout drawer;
    NavigationView navigationView;
    private FirebaseAuth mAuth;
    TextView txtTitulo, txtUserName, txtUserEmail;


    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        txtTitulo = view.findViewById(R.id.txtTitulo);
        txtTitulo.setText("Dashboard");
        //menu
        mAuth = FirebaseAuth.getInstance();


        drawer = view.findViewById(R.id.drawer_layout);
        navigationView = view.findViewById(R.id.nav_view);
        btnToolbar = view.findViewById(R.id.btnToolbar);
        btnToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);

            }
        });
        View headerView = navigationView.getHeaderView(0);



        userPhoto = headerView.findViewById(R.id.userPhoto);
        txtUserName = headerView.findViewById(R.id.txtUsername);
        txtUserEmail = headerView.findViewById(R.id.txtEmail);
        txtUserName.setText(mAuth.getCurrentUser().getDisplayName());
        txtUserEmail.setText(mAuth.getCurrentUser().getEmail());
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.nav_home:
                        Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                new HomeFragment(), null).addToBackStack(null).commit();
                        drawer.closeDrawers();
                        txtTitulo.setText("Dashboard");
                        return true;
                    case R.id.nav_classroom:
                        Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                new ClassroomFragment(), null).addToBackStack(null).commit();
                        drawer.closeDrawers();
                        txtTitulo.setText("Classroom");
                        return true;
                    case R.id.nav_lessons:
                        Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                new LessonsFragment(), null).addToBackStack(null).commit();
                        drawer.closeDrawers();
                        txtTitulo.setText("Lessons");
                        return true;
                    case R.id.nav_my_profile:
                        Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                                new ProfileFragment(), null).addToBackStack(null).commit();
                        drawer.closeDrawers();
                        txtTitulo.setText("Profile");
                        return true;

                    case R.id.nav_logout:
                        mAuth.signOut();
                        LoginManager.getInstance().logOut();
                        Intent intent = new Intent(getContext(),MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        updateUI(null);
                        return true;
                }
                return false;
            }
        });
        // Inflate the layout for this fragment
        return view;

    }
    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent(getContext(), Home.class);
            startActivity(intent);
            //  User is signed in
        }
    }


}

