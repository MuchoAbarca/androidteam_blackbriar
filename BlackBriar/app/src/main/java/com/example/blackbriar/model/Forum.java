package com.example.blackbriar.model;

public class Forum {
    private String uidForum;
    private String nameForum;
    private String descripción;
    private String diaInicio;
    private String diaFinal;
    private int inscritos;
    private int pointsHealer;
    private int pointsWarlock;

    public Forum(String uidForum, String nameForum, String descripción, String diaInicio, String diaFinal, int inscritos, int pointsHealer, int pointsWarlock) {
        this.uidForum = uidForum;
        this.nameForum = nameForum;
        this.descripción = descripción;
        this.diaInicio = diaInicio;
        this.diaFinal = diaFinal;
        this.inscritos = inscritos;
        this.pointsHealer = pointsHealer;
        this.pointsWarlock = pointsWarlock;
    }
    public Forum(){

    }

    public String getUidForum() {
        return uidForum;
    }

    public void setUidForum(String uidForum) {
        this.uidForum = uidForum;
    }

    public String getNameForum() {
        return nameForum;
    }

    public void setNameForum(String nameForum) {
        this.nameForum = nameForum;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(String diaInicio) {
        this.diaInicio = diaInicio;
    }

    public String getDiaFinal() {
        return diaFinal;
    }

    public void setDiaFinal(String diaFinal) {
        this.diaFinal = diaFinal;
    }

    public int getInscritos() {
        return inscritos;
    }

    public void setInscritos(int inscritos) {
        this.inscritos = inscritos;
    }

    public int getPointsHealer() {
        return pointsHealer;
    }

    public void setPointsHealer(int pointsHealer) {
        this.pointsHealer = pointsHealer;
    }

    public int getPointsWarlock() {
        return pointsWarlock;
    }

    public void setPointsWarlock(int pointsWarlock) {
        this.pointsWarlock = pointsWarlock;
    }
}
