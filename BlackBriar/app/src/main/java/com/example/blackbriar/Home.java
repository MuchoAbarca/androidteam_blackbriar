package com.example.blackbriar;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Home extends AppCompatActivity {

    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        fragmentManager=getSupportFragmentManager();
        //Overalping fragment if not
        if(savedInstanceState!=null){
            return;
        }
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        // se crea objeto homeFragment que se agrega al fragment_conteiner
        DashboardFragment dashboardFragment= new DashboardFragment();
        HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.add(R.id.fragment_container_home,dashboardFragment,null);
        fragmentTransaction.add(R.id.fragment_container_dashboard,homeFragment,null );
        fragmentTransaction.commit();

    }
}
