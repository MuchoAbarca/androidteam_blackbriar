package com.example.blackbriar;

import android.app.Application;

public class Variables extends Application{
    private String uidForum;
    private String titleGroup;

    public String getUidForum() {
        return uidForum;
    }

    public void setUidForum(String uidForum) {
        this.uidForum = uidForum;
    }

    public String getTitleGroup() {
        return titleGroup;
    }

    public void setTitleGroup(String titleGroup) {
        this.titleGroup = titleGroup;
    }
}
