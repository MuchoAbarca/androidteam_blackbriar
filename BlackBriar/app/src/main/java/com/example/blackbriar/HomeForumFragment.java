package com.example.blackbriar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeForumFragment extends Fragment {

    View view;
    TextView txtTitleForum;
    String title;

    public HomeForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_forum, container, false);{
            Variables titleForum =(Variables) getApplicationContext();
        txtTitleForum = view.findViewById(R.id.txtTitleForum);
        title = titleForum.getTitleGroup();
        txtTitleForum.setText(title);
        }
        // Inflate the layout for this fragment
        return view;
    }

}
