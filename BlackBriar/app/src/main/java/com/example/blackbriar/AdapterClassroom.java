package com.example.blackbriar;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.blackbriar.model.Group;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static java.lang.String.valueOf;

public class AdapterClassroom extends RecyclerView.Adapter<Adapter.GroupViewholder>{

    public AdapterClassroom(List<Group> groups) {
        this.groups = groups;
    }

    private FirebaseUser currentUser;

    FirebaseFirestore db;

    private List<Group> groups;

    @NonNull
    @Override
    public Adapter.GroupViewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.groupcard, viewGroup, false);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        currentUser = mAuth.getCurrentUser();
        Adapter.GroupViewholder holder = new Adapter.GroupViewholder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(@NonNull final Adapter.GroupViewholder groupViewholder, int i) {

        Group group = groups.get(i);
        groupViewholder.txtName.setText(group.getName());
        groupViewholder.txtPropietario.setText(group.getPropietario());
        final String userNumber = valueOf(group.getCantidadAlumnos());
        final String NameGroup = valueOf(group.getName());
        final String propietario = valueOf(group.getPropietario());
        final String uidGroup = valueOf(group.getUid());
        String userRegist = valueOf(group.getRegistrados());
        String total = userRegist + "/" + userNumber;
        final String typeOfGroup = valueOf(group.getPrivado());
        groupViewholder.txtNUsers.setText(total);
        if (typeOfGroup.equals("true")) {
            groupViewholder.privateView.setVisibility(View.VISIBLE);
        }
        groupViewholder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Variables vForum =  (Variables) getApplicationContext();
                vForum.setUidForum(uidGroup);
                vForum.setTitleGroup(NameGroup);
                Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_dashboard,
                        new ForumFragment(), null).addToBackStack(null).commit();
                Home.fragmentManager.beginTransaction().replace(R.id.fragment_container_forum,
                        new HomeForumFragment(), null).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

}
